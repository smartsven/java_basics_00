/**
 * Copyright (c) 2020-2024 Sven Gothel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package sic.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * Siehe Java Grundelemente, JUC2 01.04 Java01.
 * <p>
 * Basic integer arithmetic part 1.
 * </p>
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class Test0104Java01Aufgabe01 {
    /**
     * TODO: Gebe die Summe der ersten `n+1` integer werte
     * beginnend von `0` zurueck.
     * <pre>
     * Z.B.
     * - n==0 -> { 0 }       -> 0     -> return 0
     * - n==1 -> { 0, 1 }    -> 0+1   -> return 1
     * - n==2 -> { 0, 1, 2 } -> 0+1+2 -> return 3
     * </pre>
     * <p>
     * Note: Simple loop solution resolves in linear time, i.e. O(n)
     * </p>
     */
    static int summe_a(final int n) {
        int sum = 0;
        for(int i=0; i<=n; ++i) {
            sum += i;
        }
        return sum;
    }

    /**
     * Test fuer {@link #summe_a(int)}.
     */
    @Test
    void test01_summe() {
        Assertions.assertEquals( 0, summe_a(0));
        Assertions.assertEquals( 1, summe_a(1));
        Assertions.assertEquals( 3, summe_a(2));
        Assertions.assertEquals( 6, summe_a(3));
        Assertions.assertEquals(28, summe_a(7));
    }

    /**
     * TODO: Gebe die Summe der integer werte von [`a`..`b`] zurueck,
     * <pre>
     * Z.B.
     * - a==0, b==2 -> { 0, 1, 2 } -> 0+1+2 -> return 3
     * - a==2, b==4 -> { 2, 3, 4 } -> 2+3+4 -> return 9
     * </pre>
     * <p>
     * Note: Simple loop solution resolves in linear time, i.e. O(n)
     * </p>
     */
    static int summe_b(final int a, final int b) {
        int sum = 0;
        // gegeben a = 3
        // gegeben b = 5
        // var sum = 0
        // var i = a = 3 ('for' init)
        // 1: i<=b -> 3 <= 5 ? -> true
        // 1: sum: 0 -> 0+3 -> 3
        // 1: i = i + 1 = 3 + 1 = 4
        //
        // 2: i<=b -> 4 <= 5 ? -> true
        // 2: sum: 3 -> 3+4 -> 7
        // 2: i = i + 1 = 4 + 1 = 5
        //
        // 3: i<=b -> 5 <= 5 ? -> true
        // 3: sum: 7 -> 7+5 -> 12
        // 3: i = i + 1 = 5 + 1 = 6
        //
        // 4: i<=b -> 6 <= 5 ? -> false ('for' exit)
        // return sum -> 12
        //
        // 0: sum: 0
        // 1: sum: 0 -> 0+3 -> 3
        // 2: sum: 3 -> 3+4 -> 7
        // 3: sum: 7 -> 7+5 -> 12
        // 
        // sum = 0;
        // sum = 0+3+4+5
        //
        // fuer alle i von a bis b inklusive, lasse
        //     sum = sum + i
        // return sum
        //
        for(int i=a; i<=b; ++i) {
            sum += i;
        }
        return sum;
    }

    /**
     * Friedrich Gauss Summenformel 'Little Gauss'
     *
     *   1+2+3+4+5+6
     *
     *   a         b
     *   1 2 3 4 5 6
     *       3+4      =  7
     *     2  +  5    =  7
     *   1    +    6  =  7
     *   a    +    b
     *   1+2+3+4+5+6  = 21
     *
     *   6 / 2 = 3
     *   b / 2
     *
     *   (a + b) * b/2
     *
     *   1+2+3+4+5+6  = 3 * 7
     *
     *   a    +    b
     *   1    +    6  = 7
     *
     * --------------------
     *
     *   1+2+3+4+5+6+7+8
     *
     *   a             b
     *   1 2 3 4 5 6 7 8
     *         4+5       =  9
     *       3  +  6     =  9
     *     2    +    7   =  9
     *   1      +      8 =  9
     *   a      +      b
     *   1+2+3+4+5+6+7+8 = 36
     *
     *   8 / 2 = 4
     *   b / 2
     *
     *   (a + b) * b/2
     *
     * --------------------
     *
     *   1+2+3+4+5 = 15
     *
     *   a       b
     *   1 2 3 4 5
     *
     *       3        =  3
     *     2 + 4      =  6
     *   1   +   5    =  6
     *   a       b
     *   1+2+3+4+5    = 15
     *
     *   n = b-a+1 == b, mit a==1
     *   n = 6 -> n/2 = 6/2 = 3
     *
     *   5 / 2 = 2 + 1/2
     *   b / 2
     *
     *   (a + b) * b/2
     *
     *   = 6 * ( 2 + 1/2 )
     *   = 6 * 2 + 6 * 1/2
     *   = 12 + 3
     *   = 15
     *
     * --------------------
     * --------------------
     *
     *   3+4+5+6+7+8
     *
     *   a         b
     *   3 4 5 6 7 8
     *       5+6       = 11
     *     4  +  7     = 11
     *   3    +    8   = 11
     *   a    +    b
     *   3+4+5+6+7+8   = 33
     *
     *   n = b-a+1
     *   n = 6 -> n/2 = 6/2 = 3
     *
     *   (3 + 8) * 3   = 33
     *
     *   (a + b) * (b-a+1)/2
     *
     * --------------------
     *
     *   3+4+5+6+7
     *
     *   a       b
     *   3 4 5 6 7
     *       5        =  5
     *     4 + 6      = 10
     *   3   +   7    = 10
     *   a   +   b
     *   3+4+5+6+7    = 25
     *
     *   n = b-a+1
     *   n = 7-3+1 = 5
     *   n = 5 -> n/2 = 5/2 = 2 + 1/2
     *
     *  1:   (3 + 7) * 5/2
     *  2: ( (3 + 7) * 5 ) /2
     *
     *  1:   (3 + 7) *   5 / 2     = 25
     *  1:   (3 + 7) * ( 2 + 1/2 ) = 25
     *  1:        10 * ( 2 + 1/2 ) = 25
     *  1:       10 * 2 + 10 * 1/2 = 25
     *  1:                  20 + 5 = 25
     *  1:   (a + b) * (b-a+1)/2
     *
     *  2: ( (3 + 7) * 5 ) / 2     = 25
     *  2: (      10 * 5 ) / 2     = 25
     *  2: (          50 ) / 2     = 25
     *  2:                  25     = 25
     *  2: ( (a + b) * (b-a+1) ) / 2
     *
     * @param a
     * @param b
     * @return
     */
    static int summe_c(final int a, final int b) {
        return ( ( a + b ) * ( b - a + 1 ) ) / 2;
    }

    /**
     * Test fuer {@link #summe_b(int, int)}
     */
    @Test
    void test02_summe() {
        {
            Assertions.assertEquals( 0, summe_b(0, 0));

            Assertions.assertEquals( 1, summe_b(0, 1));
            Assertions.assertEquals( 3, summe_b(0, 2));
            Assertions.assertEquals( 3, summe_b(1, 2));
            Assertions.assertEquals( 6, summe_b(0, 3));
            Assertions.assertEquals( 6, summe_b(1, 3));
            Assertions.assertEquals( 5, summe_b(2, 3));
            Assertions.assertEquals(28, summe_b(0, 7));
            Assertions.assertEquals(28, summe_b(1, 7));
            Assertions.assertEquals(27, summe_b(2, 7));
            Assertions.assertEquals(18, summe_b(5, 7));
            Assertions.assertEquals(13, summe_b(6, 7));

            Assertions.assertEquals( 9, summe_b(2, 4));
        }
        {
            Assertions.assertEquals( 0, summe_c(0, 0));

            Assertions.assertEquals( 1, summe_c(0, 1));
            Assertions.assertEquals( 3, summe_c(0, 2));
            Assertions.assertEquals( 3, summe_c(1, 2));
            Assertions.assertEquals( 6, summe_c(0, 3));
            Assertions.assertEquals( 6, summe_c(1, 3));
            Assertions.assertEquals( 5, summe_c(2, 3));
            Assertions.assertEquals(28, summe_c(0, 7));
            Assertions.assertEquals(28, summe_c(1, 7));
            Assertions.assertEquals(27, summe_c(2, 7));
            Assertions.assertEquals(18, summe_c(5, 7));
            Assertions.assertEquals(13, summe_c(6, 7));

            Assertions.assertEquals( 9, summe_c(2, 4));
        }
    }

    /**
     * TODO: Gebe das Produkt der integer werte von `a` * `b` zurueck,
     * _OHNE_ die eingebaute multiplikation zu verwenden, d.h. nur mit addition.
     * <pre>
     * Z.B.
     * - a==2, b==3 -> 2 * 3 -> return 6
     * </pre>
     */
    static int mul_a(final int a, final int b) {
        int r = 0;
        /***
         * Without optimizations:
         *
            for(int i=0; i<a; ++i) {
                r += b; // r=r+b
            }
            return r;
         *
         */

        // With optimizations (Aslihan Oksuz)
        if(a == 0 || b == 0) {
            return 0;
        }else if(a < b) {
            for(int i = 1; i <= a; ++i) {
                r += b;
            }
        }else{
            for(int i = 1; i <= b; ++i) {
                r += a;
            }
        }
        return r;
    }

    /**
     * Test fuer {@link #mul_a(int, int)}
     */
    @Test
    void test03_mul() {
        Assertions.assertEquals( 0, mul_a(0, 0));
        Assertions.assertEquals( 1, mul_a(1, 1));
        Assertions.assertEquals( 2, mul_a(1, 2));
        Assertions.assertEquals( 6, mul_a(2, 3));
        Assertions.assertEquals(28, mul_a(4, 7));

        Assertions.assertEquals( 9, mul_a(3, 3));
    }

    /**
     * TODO: Return the modulo-value of the integer value of `a` % `b`,
     * _WITHOUT_ using the build-in modulo-operator `%`.
     * <pre>
     * Z.B.
     * - a==5, b==2 -> 5 % 2 -> return 1
     * </pre>
     */
    static int modulo_a(final int a, final int b) {
        /**
         * O(n) path using manual division w/o build-in '/' and '*' operations (Jan Reinicke)
         *
            int result = a;
            while (result >= b) {
                result -= b;
            }
            return result;
         */
        // Desired fast path O(1) using build-in '/' and '*' operations.
        return a - ( a / b ) * b; // Modulo definition, i.e. Divisionsrest
    }

    /**
     * Test fuer {@link #modulo_a(int, int)}
     */
    @Test
    void test04_modulo() {
        Assertions.assertEquals( 0, modulo_a(0, 1));
        Assertions.assertEquals( 0, modulo_a(1, 1));
        Assertions.assertEquals( 1, modulo_a(1, 2));
        Assertions.assertEquals( 2, modulo_a(2, 3));
        Assertions.assertEquals( 0, modulo_a(3, 3));
        Assertions.assertEquals( 1, modulo_a(4, 3));
        Assertions.assertEquals( 3, modulo_a(7, 4));
    }

    /**
     * TODO: Return the exponential-value of the integer value of `a` ^ `b`,
     * _WITHOUT_ using the pow() library function.
     * <pre>
     * Z.B.
     * - a==5, b==2 -> 5 ^ 2 == 5 * 5 -> return 25
     * </pre>
     */
    static int pow_a(final int a, final int b) {
        int r=1;
        for (int i=0; i<b; ++i){
            r*=a; // r = r * x
        }
        return r;
    }

    /**
     * Test fuer {@link #pow_a(int, int)}
     */
    @Test
    void test05_pow() {
        Assertions.assertEquals( 0, pow_a(0, 1));
        Assertions.assertEquals( 0, pow_a(0, 2));

        Assertions.assertEquals(  1, pow_a(-1, 0));
        Assertions.assertEquals(  1, pow_a(-2, 0));
        Assertions.assertEquals(  1, pow_a(-3, 0));
        Assertions.assertEquals(  1, pow_a( 0, 0));
        Assertions.assertEquals(  1, pow_a( 1, 0));
        Assertions.assertEquals(  1, pow_a( 2, 0));
        Assertions.assertEquals(  1, pow_a( 3, 0));

        Assertions.assertEquals( -1, pow_a(-1, 1));
        Assertions.assertEquals( -2, pow_a(-2, 1));
        Assertions.assertEquals( -3, pow_a(-3, 1));
        Assertions.assertEquals(  0, pow_a( 0, 1));
        Assertions.assertEquals(  1, pow_a( 1, 1));
        Assertions.assertEquals(  2, pow_a( 2, 1));
        Assertions.assertEquals(  3, pow_a( 3, 1));

        Assertions.assertEquals(  1, pow_a(-1, 2));
        Assertions.assertEquals(  4, pow_a(-2, 2));
        Assertions.assertEquals(  9, pow_a(-3, 2));
        Assertions.assertEquals(  0, pow_a( 0, 2));
        Assertions.assertEquals(  1, pow_a( 1, 2));
        Assertions.assertEquals(  4, pow_a( 2, 2));
        Assertions.assertEquals(  9, pow_a( 3, 2));

        Assertions.assertEquals( -1, pow_a(-1, 3));
        Assertions.assertEquals( -8, pow_a(-2, 3));
        Assertions.assertEquals(-27, pow_a(-3, 3));
        Assertions.assertEquals(  0, pow_a( 0, 3));
        Assertions.assertEquals(  1, pow_a( 1, 3));
        Assertions.assertEquals(  8, pow_a( 2, 3));
        Assertions.assertEquals( 27, pow_a( 3, 3));
    }

}
