/**
 * Copyright (c) 2020-2024 Sven Gothel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package sic.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * Siehe Java Grundelemente, JUC2 01.04 Java01.
 * <p>
 * Part 4: Basic algorithms String: Number Conversion
 * </p>
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class Test0104Java01Aufgabe04 {
    public static String toBinaryString(final int number) {
        String res = null;
        {
            final StringBuilder sb = new StringBuilder();
            int remainder = number;
            while(0 != remainder) {
                sb.append( 1 == ( remainder & 1 ) ? '1' : '0' ); // oder 1 == ( remainder % 2 )
                remainder >>>= 1; // 1 right bit-shift remainder -> remainder = remainder / 2
            }
            if( sb.isEmpty() ) {
                res = "0";
            } else {
                res = sb.reverse().toString();
            }
            System.out.printf("toBinaryString: 0b%s -> 0b%s%n", Integer.toBinaryString(number), res);
        }
        return res;
    }
    /**
     * Convert given number to a string with given thousand separator
     * @param number number to be converted to a string
     * @param separator thousand separator, i.e. inserted for every power of thousand, e.g. pow(thousand, n).
     * @return string representation of given number
     */
    public static String toDecimalString(final int number, final char separator) {
        String res = null;
        {
            final StringBuilder sb = new StringBuilder();
            int remainder = number;
            int nextSeparator = 3; // every 3 digits one thousand separator
            while(0 != remainder) {
                final int digit = remainder % 10; // digit within [0..9]
                sb.append( (char)('0' + digit) );
                remainder /= 10;
                if( 0 == --nextSeparator ) {
                    nextSeparator = 3;
                    sb.append( separator );
                }
            }
            if( sb.isEmpty() ) {
                res = "0";
            } else {
                res = sb.reverse().toString();
            }
            System.out.printf("toDecimalString: %d -> %s%n", number, res);
        }
        return res;
    }
    public static String toHexString(final int number, final boolean lowerCase) {
        String res = null;
        {
            final StringBuilder sb = new StringBuilder();
            int remainder = number;
            while(0 != remainder) {
                final int digit = remainder & 0x000f; // digit within [0..15], i.e. 0b00001111 (lowest 4 bits) -> digit = remainder % 16
                if( 10 > digit ) {
                    sb.append( (char)('0' + digit) );
                } else if( lowerCase ) {
                    sb.append( (char)('a' + digit - 10) );
                } else {
                    sb.append( (char)('A' + digit - 10) );
                }
                remainder >>>= 4; // 4 right bit-shifts of remainder -> remainder = remainder / 16
            }
            if( sb.isEmpty() ) {
                res = "0";
            } else {
                res = sb.reverse().toString();
            }
            System.out.printf("toHexString: 0x%x -> 0x%s%n", number, res);
        }
        return res;
    }

    @Test
    void test01ToBaseString() {
        if( true ) {
            Assertions.assertEquals(Integer.toBinaryString(0b00000000000000000000000000000000),
                                            toBinaryString(0b00000000000000000000000000000000));
            Assertions.assertEquals(Integer.toBinaryString(0b00000000000000000000000000000001),
                                            toBinaryString(0b00000000000000000000000000000001));
            Assertions.assertEquals(Integer.toBinaryString(0b00000000000000000000000000000010),
                                            toBinaryString(0b00000000000000000000000000000010));
            Assertions.assertEquals(Integer.toBinaryString(0b00000000000000000000000000000011),
                                            toBinaryString(0b00000000000000000000000000000011));
            Assertions.assertEquals(Integer.toBinaryString(0b01010101010101010101010101010101),
                                            toBinaryString(0b01010101010101010101010101010101));
            Assertions.assertEquals(Integer.toBinaryString(0b10101010101010101010101010101010),
                                            toBinaryString(0b10101010101010101010101010101010));
            Assertions.assertEquals(Integer.toBinaryString(0b10101111100001110000111001101011),
                                            toBinaryString(0b10101111100001110000111001101011));
            Assertions.assertEquals(Integer.toBinaryString(0b11111111111111111111111111111111),
                                            toBinaryString(0b11111111111111111111111111111111));
        }
        if( false ) { // FIXME: activate
            Assertions.assertEquals(         "0", toDecimalString(       0, '\''));
            Assertions.assertEquals(    "10'000", toDecimalString(   10000, '\''));
            Assertions.assertEquals(    "99'999", toDecimalString(   99999, '\''));
            Assertions.assertEquals( "9.111.999", toDecimalString( 9111999, '.'));
            Assertions.assertEquals("11.111.111", toDecimalString(11111111, '.'));
        }
        if( false ) { // FIXME: activate
            Assertions.assertEquals(Integer.toHexString(0x0000),               toHexString(0x0000, true));
            Assertions.assertEquals(Integer.toHexString(0x0001),               toHexString(0x0001, true));
            Assertions.assertEquals(Integer.toHexString(0x0009),               toHexString(0x0009, true));
            Assertions.assertEquals(Integer.toHexString(0x000a),               toHexString(0x000a, true));
            Assertions.assertEquals(Integer.toHexString(0x000f),               toHexString(0x000f, true));
            Assertions.assertEquals(Integer.toHexString(0x00FF),               toHexString(0x00FF, true));
            Assertions.assertEquals(Integer.toHexString(0x1111),               toHexString(0x1111, true));
            Assertions.assertEquals(Integer.toHexString(0xaaaa),               toHexString(0xaaaa, true));
            Assertions.assertEquals(Integer.toHexString(0xaffe),               toHexString(0xaffe, true));
            Assertions.assertEquals(Integer.toHexString(0xaffe).toUpperCase(), toHexString(0xaffe, false));
            Assertions.assertEquals(Integer.toHexString(0xFF00),               toHexString(0xFF00, true));
            Assertions.assertEquals(Integer.toHexString(0xFFFF),               toHexString(0xFFFF, true));
        }
    }

    //
    //
    //

    public static int fromBinaryString(final String numStr) {
        int res = 0;
        {
            final int skip = numStr.startsWith("0b") ? 2 : 0;
            final int numStrLen = numStr.length();
            for(int i=0; i<numStrLen-skip; ++i) {
                final char c = numStr.charAt( numStrLen - 1 - i); // right to left
                if( '1' == c ) {
                    res |= 1 << i;  // 1 bit-shift-left -> multiply by 2 (base 2)
                } else if( '0' != c ) {
                    break; // end of valid binary number string
                }
            }
            System.out.printf("fromBinaryString: %s -> 0b%s%n", numStr, Integer.toBinaryString(res));
        }
        return res;
    }
    public static int fromHexString(final String numStr) {
        int res = 0;
        {
            final int skip = numStr.startsWith("0x") ? 2 : 0;
            final int numStrLen = numStr.length();
            for(int i=0; i<numStrLen-skip; ++i) {
                final char c = numStr.charAt( numStrLen - 1 - i); // right to left
                if( '0' <= c && c <= '9' ) {
                    res |= (c - '0') << (i*4); // 4 bit-shifts-left -> multiply by 16 (base 16)
                } else if( 'a' <= c && c <= 'f' ) {
                    res |= (c - 'a' + 10) << (i*4); // 4 bit-shifts-left -> multiply by 16 (base 16)
                } else if( 'A' <= c && c <= 'F' ) {
                    res |= (c - 'A' + 10) << (i*4); // 4 bit-shifts-left -> multiply by 16 (base 16)
                } else {
                    break; // end of valid binary number string
                }
            }
            System.out.printf("fromHexString: %s -> 0b%s%n", numStr, Integer.toHexString(res));
        }
        return res;
    }

    @Test
    void test02FromBaseString() {
        if( true ) {
            Assertions.assertEquals(0b00000000000000000000000000000000,
                                    fromBinaryString("0b00000000000000000000000000000000"));
            Assertions.assertEquals(0b00000000000000000000000000000001,
                                    fromBinaryString("0b00000000000000000000000000000001"));
            Assertions.assertEquals(0b00000000000000000000000000000010,
                                    fromBinaryString("0b00000000000000000000000000000010"));
            Assertions.assertEquals(0b00000000000000000000000000000011,
                                    fromBinaryString("0b00000000000000000000000000000011"));
            Assertions.assertEquals(0b01010101010101010101010101010101,
                                    fromBinaryString("0b01010101010101010101010101010101"));
            Assertions.assertEquals(0b10101010101010101010101010101010,
                                    fromBinaryString("0b10101010101010101010101010101010"));
            Assertions.assertEquals(0b10101111100001110000111001101011,
                                    fromBinaryString("0b10101111100001110000111001101011"));
            Assertions.assertEquals(0b11111111111111111111111111111111,
                                    fromBinaryString("0b11111111111111111111111111111111"));

            Assertions.assertEquals(0b01010101010101010101010101010101,
                                    fromBinaryString("01010101010101010101010101010101"));
            Assertions.assertEquals(0b10101010101010101010101010101010,
                                    fromBinaryString("10101010101010101010101010101010"));
            Assertions.assertEquals(0b10101111100001110000111001101011,
                                    fromBinaryString("10101111100001110000111001101011"));
            Assertions.assertEquals(0b11111111111111111111111111111111,
                                    fromBinaryString("11111111111111111111111111111111"));
        }
        if( true ) { // FIXME: activate
            Assertions.assertEquals(0x0000, fromHexString("0x0000"));
            Assertions.assertEquals(0x0001, fromHexString("0x0001"));
            Assertions.assertEquals(0x0009, fromHexString("0x0009"));
            Assertions.assertEquals(0x000a, fromHexString("0x000a"));
            Assertions.assertEquals(0x000f, fromHexString("0x000f"));
            Assertions.assertEquals(0x00FF, fromHexString("0x00FF"));
            Assertions.assertEquals(0x1111, fromHexString("0x1111"));
            Assertions.assertEquals(0xaaaa, fromHexString("0xaaaa"));
            Assertions.assertEquals(0xaffe, fromHexString("0xaffe"));
            Assertions.assertEquals(0xaffe, fromHexString("0xaffe"));
            Assertions.assertEquals(0xFF00, fromHexString("0xFF00"));
            Assertions.assertEquals(0xFFFF, fromHexString("0xFFFF"));

            Assertions.assertEquals(0xaaaa, fromHexString("aaaa"));
            Assertions.assertEquals(0xaffe, fromHexString("affe"));
            Assertions.assertEquals(0xaffe, fromHexString("affe"));
            Assertions.assertEquals(0xFF00, fromHexString("FF00"));
            Assertions.assertEquals(0xFFFF, fromHexString("FFFF"));
        }
    }

}
