package sic.test;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class Test0104Java01LN01 {

    @Test
    void test01_basic_integer_arithmetik() {
        {
            final int a = 1;
            final int b = 2;
            final int c = a + b; // FIXED operation
            Assertions.assertEquals(3, c);
        }
        {
            final int i = 6;
            final int j = 2;
            final int k = i * j; // FIXED operation
            Assertions.assertEquals(12, k);
        }
        {
            final int i = +6;
            final int j = -2;
            final int k = 2*(i + j); // FIXED operation
            Assertions.assertEquals(8, k);
        }
        {
            final int i = 7;
            final int j = 2;
            final int k = i - j; // FIXED operation
            Assertions.assertEquals(5, k);
        }
        {
            final int i = 3;
            final int j = 7;
            final int k = i * j; // FIXED operation
            Assertions.assertEquals(21, k);
        }
        {
            final int i = 64;
            final int j = 4;
            final int k = i / j; // FIXED operation
            Assertions.assertEquals(16, k);
        }
        {
            final int i = 2; // FIXED operand
            final int j = 3; // FIXED operand
            final int k = i % j;
            Assertions.assertEquals(2, k);
        }
    }

    @Test
    void test11_pre_post_increment() {
        int i = 1;
        int j = 1;
        final int c1 = ++i;
        final int c2 = j++;
        final int exp_c1 = 2; // FIXED expected result for c1
        final int exp_c2 = 1; // FIXED expected result for c2
        Assertions.assertEquals( exp_c1, c1);
        Assertions.assertEquals( exp_c2, c2);
    }

    @Test
    void test12_assignment() {
        {
            int i = 6; // FIXED assigned value
            Assertions.assertEquals(6, i);
            i = 2; // FIXED assigned value
            Assertions.assertEquals(2, i);
        }

        {
            int i = 8;
            i += 2; // FIXED
            Assertions.assertEquals(10, i);
        }
        {
            int i = 9;
            i -= 7; // FIXED
            Assertions.assertEquals(2, i);
        }
        {
            int i = 3;
            i *= 4; // FIXED
            Assertions.assertEquals(12, i);
        }
        {
            int i = 128;
            i /= 4; // FIXED
            Assertions.assertEquals(32, i);
        }
        {
            {
                int i = 6;
                i %= 3; // FIXED
                Assertions.assertEquals(0, i);
            }
            {
                int i = 5;
                i %= 4; // FIXED
                Assertions.assertEquals(1, i);
            }
        }
    }

    @Test
    void test13_equality_relational() {
        {
            final int i = 6;
            final int j = 6;
            final int k = 3;
            final boolean exp_1 = true;   // FIXED expected result 1
            final boolean exp_2 = false;  // FIXED expected result 2
            final boolean exp_3 = false;  // FIXED expected result 3
            final boolean exp_4 = true;   // FIXED expected result 4

            Assertions.assertEquals(exp_1, i == j);
            Assertions.assertEquals(exp_2, i != j);

            Assertions.assertEquals(exp_3, i == k);
            Assertions.assertEquals(exp_4, i != k);
        }
        {
            final int i = 6;
            final int j = 6;
            final int k = 7;

            final boolean exp_1 = false;   // FIXED expected result 1
            final boolean exp_2 = true;    // FIXED expected result 2
            final boolean exp_3 = true;    // FIXED expected result 3
            final boolean exp_4 = false;   // FIXED expected result 4
            Assertions.assertEquals(false, i <  j);
            Assertions.assertEquals(true,  i <= j);
            Assertions.assertEquals(true,  i >= j);
            Assertions.assertEquals(false, i >  j);

            final boolean exp_5 = true;    // FIXED expected result 1
            final boolean exp_6 = true;    // FIXED expected result 2
            final boolean exp_7 = false;   // FIXED expected result 3
            final boolean exp_8 = false;   // FIXED expected result 4
            Assertions.assertEquals(exp_5,  i <  k);
            Assertions.assertEquals(exp_6,  i <= k);
            Assertions.assertEquals(exp_7,  i >= k);
            Assertions.assertEquals(exp_8,  i >  k);
        }
    }

    @Test
    void test14_logic_boolean() {
        {
            final int i = 6;
            final int j = 6;
            final int k = 7;
            final boolean b0 = i==j;
            final boolean b1 = i==k;

            final boolean exp_1 = true;   // FIXED expected result 1
            final boolean exp_2 = false;  // FIXED expected result 2
            final boolean exp_3 = false;  // FIXED expected result 3
            final boolean exp_4 = false;  // FIXED expected result 4
            final boolean exp_5 = true;   // FIXED expected result 5

            Assertions.assertEquals(exp_1,   b0 && !b1);
            Assertions.assertEquals(exp_2,  !( b0 && !b1) );
            Assertions.assertEquals(exp_3,  !b0 ||  b1);

            Assertions.assertEquals(exp_4,  i != j || i == k);
            Assertions.assertEquals(exp_5,  i == j && i != k);
        }
    }

    /**
     * Gebe die Summe der Integer-Werte von [`a`..`b`] zurueck,
     * - ich gebe was zurueck
     * - ich gebe eine summe zurueck
     * - summe -> addiere was
     * - summe -> addiere Zahlen von a inklusive bis b inklusive <- [`a`..`b`]
     * 
     * <pre>
     * Z.B.
     * - a==0, b==2 -> [0 .. 2] -> { 0, 1, 2 } -> 0+1+2 -> return 3
     * - a==2, b==4 -> [2 .. 4] -> { 2, 3, 4 } -> 2+3+4 -> return 9
     * </pre>
     * <p>
     * Note: Simple loop solution resolves in linear time, i.e. O(n)
     * </p>
     */
    static int summe_b(final int a, final int b) {
        int sum = 0;
        // gegeben a = 3
        // gegeben b = 5
        // var sum = 0
        // var i = a = 3 ('for' init)
        // 1: i<=b -> 3 <= 5 ? -> true
        // 1: sum: 0 -> 0+3 -> 3
        // 1: i = i + 1 = 3 + 1 = 4
        //
        // 2: i<=b -> 4 <= 5 ? -> true
        // 2: sum: 3 -> 3+4 -> 7
        // 2: i = i + 1 = 4 + 1 = 5
        //
        // 3: i<=b -> 5 <= 5 ? -> true
        // 3: sum: 7 -> 7+5 -> 12
        // 3: i = i + 1 = 5 + 1 = 6
        //
        // 4: i<=b -> 6 <= 5 ? -> false ('for' exit)
        // return sum -> 12
        //
        // 0: sum: 0
        // 1: sum: 0 -> 0+3 -> 3
        // 2: sum: 3 -> 3+4 -> 7
        // 3: sum: 7 -> 7+5 -> 12
        // 
        // sum = 0;
        // sum = 0+3+4+5
        //
        // fuer alle i von a bis b inklusive, lasse
        //     sum = sum + i
        // return sum
        //
        for(int i=a; i<=b; ++i) { // i: [a .. b]
            sum += i; // sum = sum + i;
        }
        return sum;
    }

    /**
     * Test fuer {@link #summe_b(int, int)}
     */
    @Test
    void test20_summe() {
        Assertions.assertEquals( 0, summe_b(0, 0));

        Assertions.assertEquals( 1, summe_b(0, 1));
        Assertions.assertEquals( 3, summe_b(0, 2));
        Assertions.assertEquals( 3, summe_b(1, 2));
        Assertions.assertEquals( 6, summe_b(0, 3));
        Assertions.assertEquals( 6, summe_b(1, 3));
        Assertions.assertEquals( 5, summe_b(2, 3));
        Assertions.assertEquals(28, summe_b(0, 7));
        Assertions.assertEquals(28, summe_b(1, 7));
        Assertions.assertEquals(27, summe_b(2, 7));
        Assertions.assertEquals(18, summe_b(5, 7));
        Assertions.assertEquals(13, summe_b(6, 7));

        Assertions.assertEquals( 9, summe_b(2, 4));
    }

    /**
     * Return `array` index (starting from zero)
     * of the first even number (geraden Zahl).
     *
     * Note that an even number n will have no remainder
     * if divided by zero, i.e. 0==n%2.
     *
     * @param array the given array
     * @return index of first even number if found, otherwise -1
     */
    public static int find_even(final int[] array) {
        int i=0;
        while( i<array.length ) {
            if( 0 == array[i]%2 ) {
                return i;
            }
            ++i;
        }
        return -1; // FIXED
    }
    @Test
    void test30_find_even() {
        {
            final int[] array1 = { 1, 3, 7, 5 };
            Assertions.assertEquals(-1, find_even(array1));
        }
        {
            final int[] array1 = { 1, 3, 2, 8, 7, 6, 5, 4 };
            Assertions.assertEquals(2, find_even(array1));
        }
    }

    /**
     * Return the sum of all values in given `array`.
     *
     * @param array the given array
     * @return sum of all values in given array
     */
    public static int calc_sum(final int[] array) {
        int sum = 0;
        for(int i=0; i<array.length; ++i) {
            sum += array[i];
        }
        return sum; // FIXED
    }
    @Test
    void test31_find_sum() {
        {
            final int[] array1 = { 1, 3, 7, 5 };
            Assertions.assertEquals(16, calc_sum(array1));
        }
        {
            final int[] array1 = { 1, 3, 2, 8, 7, 6, 5, 4 };
            Assertions.assertEquals(36, calc_sum(array1));
        }
    }
}
